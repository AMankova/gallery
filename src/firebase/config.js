import firebase from 'firebase/app';
import 'firebase/storage';
import 'firebase/firestore';

// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyAbUFLkwbu3Fti7FxU33atoDFEKdvCZuBM",
    authDomain: "firegram-14404.firebaseapp.com",
    databaseURL: "https://firegram-14404.firebaseio.com",
    projectId: "firegram-14404",
    storageBucket: "firegram-14404.appspot.com",
    messagingSenderId: "384664226139",
    appId: "1:384664226139:web:09b9acd40384934e9b727e"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const projectStorage = firebase.storage();
const projectFirestore = firebase.firestore();
const timestamp = firebase.firestore.FieldValue.serverTimestamp;

export { projectFirestore, projectStorage, timestamp };